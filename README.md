# Riak Persistor

This module handles accessing data in a [Riak database](https://basho.com/riak/).
This module is more or less based on the official [mongo-persistor](https://github.com/vert-x/mod-mongo-persistor) module, though heavily modified.

## Dependencies

This module requires a Riak server to be available on the network.

## Name

The module name is `riak-persistor`.

## Configuration

The module takes the following configuration:

    :::json
    {
        "address": <address>,
        "host": <host>,
        "pb_port": <port>
    }

Let's take a look at each field in turn:

* `address` The address where the module will listen for requests. Defaults to `vertx.riakpersistor`.
* `host` Host name or ip address of the Riak instance. Defaults to `localhost`.
* `pb_port` The port where Riak will listen for protocol buffers connections. Defaults to `8087`
