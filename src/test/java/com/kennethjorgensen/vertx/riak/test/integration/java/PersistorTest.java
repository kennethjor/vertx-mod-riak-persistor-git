package com.kennethjorgensen.vertx.riak.test.integration.java;
/*
 * Copyright 2013 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * Riak port by Kenneth Jorgensen.
 *
 * @author <a href="http://tfox.org">Tim Fox</a>
 * @author <a href="http://kennethjorgensen.com">Kenneth Jorgensen</a>
 */

import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.AsyncResultHandler;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.testtools.TestVerticle;

import java.util.concurrent.atomic.AtomicInteger;

import static org.vertx.testtools.VertxAssert.assertEquals;
import static org.vertx.testtools.VertxAssert.testComplete;

/**
 * Example Java integration test
 *
 * You should extend TestVerticle.
 *
 * We do a bit of magic and the test will actually be run _inside_ the Vert.x container as a Verticle.
 *
 * You can use the standard JUnit Assert API in your test by using the VertxAssert class
 */
public class PersistorTest extends TestVerticle {

	private EventBus eb;

	@Override
	public void start() {
		eb = vertx.eventBus();
		// Prepare verticle config.
		JsonObject config = new JsonObject();
		config.putString("address", "test.persistor");
		config.putString("host", System.getProperty("vertx.riak.host", "localhost"));
		config.putNumber("port", Integer.valueOf(System.getProperty("vertx.riak.port", "8087")));
		// Deploy verticle and start the tests.
		container.deployModule(System.getProperty("vertx.modulename"), config, 1, new AsyncResultHandler<String>() {
			public void handle(AsyncResult<String> ar) {
				if (ar.succeeded()) {
					PersistorTest.super.start();
				} else {
					ar.cause().printStackTrace();
				}
			}
		});
	}

	@Test
	public void testPersistor() throws Exception {
		// Store without a key.
		JsonObject json = new JsonObject()
			.putString("action", "store")
			.putString("bucket", "testbucket")
			.putObject("value", new JsonObject()
				.putString("name", "kenn")
				.putNumber("age", 28)
				.putArray("cheeses", new JsonArray(new String[]{"halloumi","cheddar"}))
			);
		eb.send("test.persistor", json, new Handler<Message<JsonObject>>() {
			public void handle(Message<JsonObject> reply) {
				JsonObject body = reply.body();
				assertEquals("ok", body.getString("status"));
				assertEquals("testbucket", body.getString("bucket"));
				assert(body.getString("key") != null);
				JsonObject val = new JsonObject(body.getString("value"));
				assertEquals("kenn", val.getString("name"));
				assertEquals(28, val.getNumber("age").intValue());
				JsonArray cheeses = val.getArray("cheeses");
				assert(cheeses != null);
				assertEquals(2, cheeses.size());
				assertEquals("halloumi", cheeses.get(0));
				assertEquals("cheddar", cheeses.get(1));
				testComplete();
			}
		});
	}
}
