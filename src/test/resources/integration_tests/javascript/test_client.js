/*
 * Copyright 2011-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Riak version by Kenneth Jorgensen.
 */

var vertx = require("vertx")
var container = require("vertx/container");
var console = require("vertx/console");
var vertxTests = require("vertx_tests");
var vassert = require("vertx_assert");

var eb = vertx.eventBus;

var isFake = false;

var persistorConfig =
{
	address: "test.persistor",
	host: java.lang.System.getProperty("vertx.riak.host", "localhost"),
	port: java.lang.Integer.valueOf(java.lang.System.getProperty("vertx.riak.port", "8087")),
	fake: isFake
}

// Deploy verticle and test database connection.
// This essentially also tests the "ping" action.
var script = this;
container.deployModule(java.lang.System.getProperty("vertx.modulename"), persistorConfig, 1, function(err, deployID) {
	if (err != null) {
		err.printStackTrace();
	} else {
		eb.send("test.persistor", {
			action: "ping"
		}, function(reply) {
			vassert.assertEquals("ok", reply.status);
			vertxTests.startTests(script);
		});
	}
});

function testSaveUpdateFetch() {
	// Store an object.
	eb.send("test.persistor", {
		action: "store",
		bucket: "testbucket",
		value: {
			name: "kenn",
			age: 28,
			pi: 3.14159,
			male: true,
			cheeses: ["halloumi", "cheddar"]
		}
	}, function(reply) {
		vassert.assertEquals("ok", reply.status);
		var key = reply.key;
		var obj = JSON.parse(reply.value);
		vassert.assertTrue(key != undefined);
		vassert.assertEquals("kenn", obj.name);
		vassert.assertEquals(28, obj.age, 0);
		vassert.assertEquals(3.14159, obj.pi, 0);
		vassert.assertEquals(true, obj.male);
		vassert.assertEquals(2, obj.cheeses.length, 0);
		vassert.assertEquals("halloumi", obj.cheeses[0]);
		vassert.assertEquals("cheddar", obj.cheeses[1]);

		// Update the same object.
		eb.send("test.persistor", {
			action: "store",
			bucket: "testbucket",
			key: key,
			value: {
				name: "kenn",
				age: 1000
			}
		}, function(reply) {
			vassert.assertEquals("ok", reply.status);
			vassert.assertEquals(key, reply.key);
			var obj = JSON.parse(reply.value);
			vassert.assertEquals("kenn", obj.name);
			vassert.assertEquals(1000, obj.age, 0);
			vassert.assertTrue(obj.pi == undefined);

			// Finally, fetch the object.
			eb.send("test.persistor", {
				action: "fetch",
				bucket: "testbucket",
				key: key
			}, function(reply) {
				vassert.assertEquals("ok", reply.status);
				var obj = JSON.parse(reply.value);
				vassert.assertEquals("kenn", obj.name);
				vassert.assertEquals(1000, obj.age, 0);

				vassert.testComplete();
			});
		});
	});
}

function testIndexes() {
	var random = Math.floor(Math.random()*1000000);
	var INDEX_INT = "testindex-int-" + random
	var INDEX_BIN = "testindex-bin-" + random;
	var n = 10;
	// Store a few objects.
	function storeOne(i, done) {
		obj = {
			action: "store",
			bucket: "testbucket",
			value: {
				i: i
			},
			indexes: {}
		}
		obj.indexes[INDEX_INT] = {
			type: "int",
			value: i
		};
		obj.indexes[INDEX_BIN] = {
			type: "bin",
			value: "string-" + i
		};
		eb.send("test.persistor", obj, function(reply) {
			vassert.assertEquals("ok", reply.status);
			//console.log("=== i:"+i+" :: "+JSON.stringify(reply));
			done();
		});
	}
	// Stores a bunch of objects.
	function storeAll(i, done) {
		if (i === null) i = 0;
		if (i === n) {
			done();
			return;
		}
		storeOne(i, function() {
			storeAll(i+1, done);
		});
	}
	// Searches the int index by value.
	function searchIntIndexValue(done) {
		// Search by int.
		var query = {
			action: "indexQuery",
			bucket: "testbucket",
			type: "int",
			name: INDEX_INT,
			value: 2
		};
		eb.send("test.persistor", query, function(reply) {
			vassert.assertEquals("ok", reply.status);
			var results = reply.results;
			vassert.assertTrue(results instanceof Array);
			vassert.assertEquals(1, results.length, 0);
			var obj = results[0];
			vassert.assertEquals(2, JSON.parse(obj.value).i, 0);
			var index = obj.indexes[INDEX_INT];
			vassert.assertEquals(2, index.values[0], 0);
			done();
		});
	}
	// Searches the bin index by value.
	function searchBinIndexValue(done) {
		// Search by int.
		var query = {
			action: "indexQuery",
			bucket: "testbucket",
			type: "bin",
			name: INDEX_BIN,
			value: "string-3"
		};
		eb.send("test.persistor", query, function(reply) {
			vassert.assertEquals("ok", reply.status);
			var results = reply.results;
			vassert.assertTrue(results instanceof Array);
			vassert.assertEquals(1, results.length, 0);
			var obj = results[0];
			vassert.assertEquals(3, JSON.parse(obj.value).i, 0);
			var index = obj.indexes[INDEX_INT];
			vassert.assertEquals(3, index.values[0], 0);
			done();
		});
	}
	// Searches the int index by value.
	function searchIntIndexRange(done) {
		// Search by int.
		var query = {
			action: "indexQuery",
			bucket: "testbucket",
			type: "int",
			name: INDEX_INT,
			from: 2,
			to: 5
		};
		eb.send("test.persistor", query, function(reply) {
			vassert.assertEquals("ok", reply.status);
			var results = reply.results;
			vassert.assertTrue(results instanceof Array);
			vassert.assertEquals(4, results.length, 0);
			var vals = [], indexes = [];
			for (var i=0 ; i<4 ; i++) {
				vals.push(JSON.parse(results[i].value).i);
				indexes.push(results[i].indexes[INDEX_INT].values[0]);
			}
			vals.sort();
			indexes.sort();
			vassert.assertEquals([2,3,4,5], vals);
			vassert.assertEquals([2,3,4,5], indexes);
			done();
		});
	}
	// Searches the int index by value.
	function searchBinIndexRange(done) {
		// Search by int.
		var query = {
			action: "indexQuery",
			bucket: "testbucket",
			type: "bin",
			name: INDEX_BIN,
			from: "string-2",
			to: "string-5"
		};
		eb.send("test.persistor", query, function(reply) {
			vassert.assertEquals("ok", reply.status);
			var results = reply.results;
			vassert.assertTrue(results instanceof Array);
			vassert.assertEquals(4, results.length, 0);
			var vals = [], indexes = [];
			for (var i=0 ; i<4 ; i++) {
				vals.push(JSON.parse(results[i].value).i);
				indexes.push(results[i].indexes[INDEX_BIN].values[0]);
			}
			vals.sort();
			indexes.sort();
			var expected = "string-2,string-3,string-4,string-5".split(",");
			vassert.assertEquals([2,3,4,5], vals);
			vassert.assertEquals(expected, indexes);
			done();
		});
	}

	// This is the actual test entry point.
	storeAll(null, function() {
		searchIntIndexValue(function() {
			searchBinIndexValue(function() {
				searchIntIndexRange(function() {
					searchBinIndexRange(function() {
						vassert.testComplete();
					});
				});
			});
		});
	});
}
